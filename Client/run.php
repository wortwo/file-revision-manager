<?php
if(file_exists(CALLER_DIR.'/.frm/project.name')) define('ACTUAL_PROJECT_NAME',file_get_contents(CALLER_DIR.'/.frm/project.name'));

function autoload($className)
{
    if(__DIR__ != ROOT_DIR) $className = str_replace(__DIR__.'/', '', $className);
    $className = preg_replace('/^Client/', '', $className);
    $className = str_replace('\\', '/', $className);
    $className = str_replace('_', '/', $className);
    if (is_file(ROOT_DIR . '' . '/' . $className . '.php')) {
        require_once(ROOT_DIR . '' . '/' . $className . '.php');
    }
}
spl_autoload_register('autoload');

//$a = new \Client\Connector\SameMachine(new \Client\Connector\Login(USER_NAME,USER_PASSWORD));
//$a->serverCommand('help');
//die();

$command = NULL;
$arguments = Array();
if (isset($argv)) {
    $arguments = $argv;
    array_shift($arguments);
    $command = array_shift($arguments);
}
if(is_null($command)) $command = 'help';
$command = '\Client\Console\\'.ucfirst($command);
$ref = new ReflectionClass($command);
if($ref->isInstantiable()){
    $command = new $command($arguments);
    $command->execute();
}