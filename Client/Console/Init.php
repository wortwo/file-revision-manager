<?php

namespace Client\Console;

use Client\Connector\File;
use Client\Connector\FileConfig;
use Client\Connector\Login;

class Init implements ICommand
{
    protected $projectName = NULL;

    public function execute()
    {
        if(is_array($this->select($this->projectName))){
            echo "Error - Project with name: '".$this->projectName."' already exists\n";
            return false;
        }

        echo "Initializing project with name:".$this->projectName."...\n";
        $save = $this->projectName."::>".serialize(Array('name' => $this->projectName,'root' => CALLER_DIR.'/'));
        $storage = new File(new FileConfig(ROOT_DIR,'projects'));
        $storage->save($save);
        mkdir(CALLER_DIR.'/.frm');
        file_put_contents(CALLER_DIR.'/.frm/project.name',$this->projectName);

        echo "Checking server for project with specifed name...\n";
        if($this->isProjectOnServer()){
            echo "Geting log from server...\n";
            file_put_contents(CALLER_DIR.'/.frm/project.log',$this->getLogFromServer());
            echo "Copying files from server...\n";
            //TODO: copy files from server
            $push = new Pull();
            $push->execute();
        }else{
            echo "This project dont exist on server!\nTrying to create new project on server...\n";
            $server = '\Client\Connector\\'.SERVER_CONNECTOR;
            $server = new $server(new Login(USER_NAME,USER_PASSWORD));
            try{
                $server->serverCommand('project', Array('create',$this->projectName,USER_NAME));
            }catch (\Exception $e){
                echo $e."\n";
            }
            $push = new Push();
            $push->execute();
        }


        file_put_contents(CALLER_DIR.'/.frm/project.head',$this->getLastLog());
        echo "DONE\n";
        die();
    }

    protected function getStorage() { return new File(new FileConfig(ROOT_DIR,'projects')); }

    protected function isProjectOnServer()
    {
        return false;
    }

    protected function getLogFromServer()
    {
        //TODO: get log from server
        return '';
    }

    protected function getLastLog()
    {
        //TODO: get log from server
        return '';
    }

    public function select($needle)
    {
        $storage = $this->getStorage();
        return $this->getDataFromString($storage->select($needle));
    }

    protected function getDataFromString($string)
    {
        if(is_null($string)) return NULL;
        $data = explode('::>',$string);
        if(count($data) != 2) throw new \Exception('Error loading data from file:'.$this->getStorage()->getFileName().' wrong data separator');
        return Array('key' => $data[0],'values' => unserialize($data[1]));
    }

    public function __construct(Array $params = Array())
    {
        $this->projectName = array_shift($params);
        if(is_null($this->projectName)){
            $this->projectName = end(explode('/',CALLER_DIR));
        }
    }
}