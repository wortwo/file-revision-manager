<?php

namespace Client\Console;

class Help extends ACommand implements ICommand
{
    public function execute()
    {
        echo "Useable commands:\n";
        foreach(glob(ROOT_DIR.'Console/*.php') as $filename){
            if($filename == ROOT_DIR.'Console/Help.php') continue;
            $filename = str_replace(ROOT_DIR.'Console/','\Client\Console\\',$filename);
            $filename = str_replace('.php','',$filename);
            $ref = new \ReflectionClass($filename);
            if($ref->isInstantiable()){
                echo "  ".lcfirst($ref->getShortName())."\n";
            }
        }
    }
}