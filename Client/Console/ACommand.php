<?php

namespace Client\Console;

abstract class ACommand
{
    protected $params = Array();

    public function __construct(Array $params = Array())
    {
        $this->params = $params;
    }
}