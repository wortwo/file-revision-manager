<?php

namespace Client\Connector;

class SameMachine implements IConnector
{
    /**
     * @var Login
     */
    protected $login = NULL;

    public function __construct(ILogin $login)
    {
        $this->login = $login;
    }

    public function serverCommand($command, Array $parameters = Array())
    {
        $command = Array(
            'php',
            SERVER_ADDRESS.'frm.php',
            'client',
            $this->login->user,
            $this->login->password,
            $command
        );
        $command = array_merge($command,$parameters);
        $exec = implode(' ',$command).';';
        $output = shell_exec($exec);
        var_dump($exec,$output);
    }

    public function sendRevision($revision)
    {
        // TODO: Implement sendRevision() method.
    }

    public function downloadRevision($revision)
    {
        // TODO: Implement downloadRevision() method.
    }
}