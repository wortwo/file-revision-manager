<?php

namespace Client\Connector;


/**
 * Class FileConfig
 * @package Server\Connector
 * @property string path
 * @property string fileName
 * @property string extension
 */
class FileConfig
{
    protected $path;
    protected $fileName;
    protected $extension;

    /**
     * @param $path
     * @param string $extension
     * @param $fileName
     */
    public function __construct($path, $fileName, $extension = 'cfg')
    {
        $this->path = trim($path);
        $this->fileName = trim($fileName);
        $this->extension = trim($extension);
        $this->checkPath();
        $this->checkExtension();
    }

    /**
     * @throws \Exception
     */
    protected function checkPath()
    {
        if(!file_exists($this->path) || !is_dir($this->path)) throw new \Exception('Path for log files did not exist "'.$this->path.'"');
    }

    protected function checkExtension()
    {
        if(!preg_match('/^\.?[a-z0-9]+$/i',$this->extension)) throw new \Exception('Invalid file extension. Extension can contain only alphabetic and numeric characters "'.$this->extension.'"');
        $this->extension = (substr($this->extension,0,1) == '.') ? $this->extension : '.'.$this->extension;
    }

    /**
     * Vraci property ro
     * @param $variableName
     * @return mixed
     */
    public function __get($variableName)
    {
        return $this->{$variableName};
    }
}