<?php

namespace Client\Connector;

/**
 * Class Login
 * @package Client\Connector
 * @property string user
 * @property string password
 */
class Login implements ILogin
{
    protected $user;
    protected $password;

    /**
     * @param $user
     * @param $password
     */
    public function __construct($user, $password)
    {
        $this->user = trim($user);
        $this->password = MD5(trim($password));
    }

    /**
     * Vraci property ro
     * @param $variableName
     * @return mixed
     */
    public function __get($variableName)
    {
        return $this->{$variableName};
    }
}