<?php

namespace Client\Connector;
use Client\Lib;

/**
 * Class File
 * @package Client\Connector
 */
class File implements IFileConnector
{
    /** @var null|\Client\Connector\FileConfig  */
    protected $fileConfig = NULL;

    /**
     * @param FileConfig $fileConfig
     */
    public function __construct(FileConfig $fileConfig)
    {
        $this->fileConfig = $fileConfig;
    }

    public function getFileName($name = NULL)
    {
        if(is_null($name)) return Lib::stringToAlphaNumeric($this->fileConfig->fileName).$this->fileConfig->extension;
        return Lib::stringToAlphaNumeric($name).$this->fileConfig->extension;
    }

    protected function getPath($additional = NULL)
    {
        if(is_null($additional)) return $this->fileConfig->path;
        $path = $this->getPath().$additional;
        if(!file_exists($path)){ mkdir($path); }
        return $path;
    }

    protected function open($mode = 'a'){
        $filePath = $this->getPath().$this->getFileName();
        if(!file_exists($filePath)) file_put_contents($filePath,'');
        $file = @fopen($filePath,$mode);
        if($file){
            return $file;
        }else{
            throw new \Exception('Error loading data file "'.$filePath.'"');
        }
    }

    public function save($string)
    {
        $file = $this->open();
        fwrite($file,$string."\n");
        fclose($file);
    }

    public function select($string)
    {
        $file = $this->open('r');
        while (!feof($file)){
            $line = fgets($file);
            if($line !== FALSE && strpos($line, $string) === 0) return $line;
        }
    }

    public function delete($string)
    {
        // TODO: Implement delete() method.
    }

    public function getAll($limit = NULL, $fromEnd = FALSE)
    {
        $lines = file($this->getPath().$this->getFileName(), FILE_IGNORE_NEW_LINES);
        if(!is_null($limit)){
            if($fromEnd){
                $lines = array_reverse(array_slice($lines,(count($lines)-$limit),$limit));
            }else{
                $lines = array_slice($lines,0,$limit);
            }
        }
        return $lines;
    }
}
