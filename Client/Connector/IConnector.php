<?php

namespace Client\Connector;

interface IConnector
{
    public function __construct(ILogin $login);
    public function serverCommand($command, Array $parameters = Array());
    public function sendRevision($revision);
    public function downloadRevision($revision);
}