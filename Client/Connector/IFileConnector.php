<?php

namespace Client\Connector;

interface IFileConnector
{
    public function __construct( FileConfig $fileConfig );
    public function save($string);
    public function select($string);
    public function delete($string);
    public function getAll($limit = NULL, $fromEnd = FALSE);
}