<?php

namespace Server\Console;

use Server\Connector\File,
    Server\Connector\FileConfig;

class User extends AParameterCommand implements IParameterCommand
{
    public function create($mail,$password)
    {
        if(is_array($this->select($mail))){
            echo "Error - User with username: '$mail' already exists\n";
            return false;
        }
        $string = $mail."::>".serialize(Array('mail' => $mail,'password' => MD5($password)));
        $storage = $this->getStorage();
        $storage->save($string);
    }

    public function showList()
    {
        $storage = $this->getStorage();
        $result = $storage->getAll();
        foreach($result as $user){
            $data = $this->getDataFromString($user);
            echo get_class($this).': '.$data['key'];
            foreach($data['values'] as $key => $value){ echo ", $key:$value"; }
            echo "\n";
        }
    }

    protected function getDataFromString($string)
    {
        if(is_null($string)) return NULL;
        $data = explode('::>',$string);
        if(count($data) != 2) throw new \Exception('Error loading data from file:'.$this->getStorage()->getFileName().' wrong data separator');
        return Array('key' => $data[0],'values' => unserialize($data[1]));
    }

    public function delete($userName)
    {

    }

    protected function getStorage() { return new File(new FileConfig(USER_FILE_PATH,USER_FILE_NAME)); }

    public function select($needle)
    {
        $storage = $this->getStorage();
        return $this->getDataFromString($storage->select($needle));
    }
}