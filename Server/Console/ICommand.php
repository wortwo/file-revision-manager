<?php

namespace Server\Console;

interface ICommand
{
    public function __construct(Array $params = Array());
    public function execute();
}