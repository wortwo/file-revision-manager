<?php

namespace Server\Console;

use Server\Connector\Login;
use Server\Listener\Console;

class Client extends ACommand implements ICommand
{

    public function execute()
    {
        if(count($this->params) < 3){
            echo "Error - Invalid number of parameters\n";
            die();
        }
        $user = array_shift($this->params);
        $password = array_shift($this->params);
        $command = array_shift($this->params);
        $listener = new Console(new Login($user,$password),$command,$this->params);
        $listener->login();
        $listener->execute();
        echo $listener->returnResult()."\n";
        die();
    }
}