<?php

namespace Server\Console;

use Server\Connector\File,
    Server\Connector\FileConfig;
use Server\Lib;

class Project extends AParameterCommand implements IParameterCommand
{
    public function create($name,$creator)
    {
        if(is_array($this->select($name))){
            echo "Error - Project with name: '$name' already exists\n";
            return false;
        }
        $user = $this->getCreator($creator);
        if(!is_array($user)){
            echo "Error - User with username: '$creator' not found\n";
            return false;
        }
        try{
            $this->createProject($name);
        }catch (\Exception $e) {
            echo $e;
            return false;
        }

        $string = $name."::>".serialize(Array('name' => $name,'creator' => $user['key']));
        $storage = $this->getStorage();
        $storage->save($string);
    }

    protected function createProject($name)
    {
        if(!file_exists(VERSION_FILES_ROOT_PATH) || !is_dir(VERSION_FILES_ROOT_PATH)) throw new \Exception('Path for version files did not exist "'.VERSION_FILES_ROOT_PATH.'" please set existing directory in config.local.php');
        $dirName = Lib::stringToAlphaNumeric($name);
        if(file_exists(VERSION_FILES_ROOT_PATH.$dirName)) throw new \Exception('Error creating project directory - directory "'.VERSION_FILES_ROOT_PATH.$dirName.'" exist. Please use different name for new project');
        mkdir(VERSION_FILES_ROOT_PATH.$dirName);
    }

    public function showList()
    {
        $storage = $this->getStorage();
        $result = $storage->getAll();
        foreach($result as $user){
            $data = $this->getDataFromString($user);
            echo get_class($this).': '.$data['key'];
            foreach($data['values'] as $key => $value){ echo ", $key:$value"; }
            echo "\n";
        }
    }

    protected function getDataFromString($string)
    {
        if(is_null($string)) return NULL;
        $data = explode('::>',$string);
        if(count($data) != 2) throw new \Exception('Error loading data from file: '.$this->getStorage()->getFileName().' wrong data separator');
        return Array('key' => $data[0],'values' => unserialize($data[1]));
    }

    public function getCreator($creator)
    {
        $user = new User(Array(AParameterCommand::COMMAND_SELECT,$creator));
        return $user->execute();
    }

    public function delete($name)
    {

    }

    protected function getStorage() { return new File(new FileConfig(PROJECT_FILE_PATH,PROJECT_FILE_NAME)); }

    public function select($needle)
    {
        $storage = $this->getStorage();
        return $this->getDataFromString($storage->select($needle));
    }
}