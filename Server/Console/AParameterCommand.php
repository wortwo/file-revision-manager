<?php

namespace Server\Console;

abstract class AParameterCommand extends ACommand implements ICommand
{
    CONST COMMAND_CREATE = 'create';
    CONST COMMAND_DELETE = 'delete';
    CONST COMMAND_LIST = 'list';
    CONST COMMAND_SELECT = 'select';

    protected $allowedCommands = Array(
        'create' => Array('create',2),
        'delete' => Array('delete',1),
        'list' => Array('showList',0),
        'select' => Array('select',1)
    );

    public function execute()
    {
        $command = array_shift($this->params);
        if(is_null($command)){
            echo "Available parameters: ".implode(', ',array_keys($this->allowedCommands))."\n";
        }elseif(array_key_exists($command,$this->allowedCommands)){
            if(count($this->params) >= $this->allowedCommands[$command][1]){
                return call_user_func_array(Array($this,$this->allowedCommands[$command][0]),$this->params);
            }else{
                echo "Invalid number of additional parameters for ".get_class($this).":$command\n";
            }
        }else{
            echo "Invalid parameter $command. Available parameters: ".implode(', ',array_keys($this->allowedCommands))."\n";
        }
    }
}