<?php

namespace Server\Console;

class Help extends ACommand implements ICommand
{
    public function execute()
    {
        echo "Useable commands:\n";
        foreach(glob('./Console/*.php') as $filename){
            if($filename == './Console/Help.php') continue;
            $filename = str_replace('./Console/','\Server\Console\\',$filename);
            $filename = str_replace('.php','',$filename);
            $ref = new \ReflectionClass($filename);
            if($ref->isInstantiable()){
                echo "  ".lcfirst($ref->getShortName())."\n";
            }
        }
    }
}