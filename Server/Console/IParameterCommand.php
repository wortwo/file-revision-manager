<?php

namespace Server\Console;

interface IParameterCommand
{
    public function create($par1,$par2);
    public function delete($par1);
    public function select($needle);
    public function showList();
}