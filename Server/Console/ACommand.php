<?php

namespace Server\Console;

abstract class ACommand implements ICommand
{
    protected $params = Array();

    public function __construct(Array $params = Array())
    {
        $this->params = $params;
    }
}