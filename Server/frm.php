<?php

include_once 'Config/config.local.php';

function autoload($className)
{
    $className = preg_replace('/^Server/', '', $className);
    $className = str_replace('\\', '/', $className);
    $className = str_replace('_', '/', $className);

    if (is_file(__DIR__ . '' . '/' . $className . '.php')) {
        require_once(__DIR__ . '' . '/' . $className . '.php');
    }
}
spl_autoload_register('autoload');

$command = NULL;
$arguments = Array();
if (isset($argv)) {
    $arguments = $argv;
    array_shift($arguments);
    $command = array_shift($arguments);
}
if(is_null($command)) $command = 'help';
$command = '\Server\Console\\'.ucfirst($command);
$ref = new ReflectionClass($command);
if($ref->isInstantiable()){
    $command = new $command($arguments);
    $command->execute();
}

