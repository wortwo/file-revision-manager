<?php

namespace Server\Listener;

use Server\Connector\ILogin,
    Server\Console\User,
    Server\Console\AParameterCommand;

class Console implements IListener
{
    /**
     * @var \Server\Connector\Login
     */
    protected $login;
    protected $logged = FALSE;
    protected $result = '';

    public function __construct(ILogin $login, $command, Array $params = Array())
    {
        $this->login = $login;
    }

    public function login()
    {
        $user = new User(Array(AParameterCommand::COMMAND_SELECT,$this->login->user));
        $result = $user->execute();
        if(is_array($result) && $this->login->password == $result['values']['password']){
            $this->logged = TRUE;
        }else{
            $this->result = 'Invalid Login';
        }
    }

    public function execute()
    {
        if(!$this->logged) return;
        // TODO: Implement execute() method.
    }

    public function returnResult() { return $this->result; }
}