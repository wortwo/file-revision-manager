<?php

namespace  Server\Listener;

use Server\Connector\ILogin;

interface IListener
{
    public function __construct(ILogin $login, $command, Array $params = Array());
    public function login();
    public function execute();
    public function returnResult();
}